#!/usr/bin/env python3
import deezer
#pip install deezer-python
import argparse
import subprocess

#smloadr_location = '/usr/bin/smloadr'

#default arguments to smloadr
quality = "MP3_320"
saveto = "$HOME/Music"
#default arguments
searchnum = 15#number of results to return

parser = argparse.ArgumentParser(description='Wrapper to smloadr with search functionality.')
parser.add_argument('type', metavar='type', type=str,
	help='type of search (artist, album, track)')
parser.add_argument('search', metavar='search', type=str,
	help='deezer search query')
parser.add_argument('-q', help='audio quality. default is '+quality)
parser.add_argument('-s', help='path to save tracks in. default is '+saveto)
parser.add_argument('-n', 
	help='number of results to return. default is {}'.format(searchnum))

args = parser.parse_args()

if args.q:
	quality = args.q
if args.s:
	saveto = args.s
if args.n:
	searchnum = args.n

validtypes = ['track', 'album', 'artist']
if args.type not in validtypes:
	print("Invalid search type")
	print("Search type must be one of \"track\", \"album\", \"artist\"")
	quit()

client = deezer.Client()

def confirm(message):
	usrin = input(message+" [y/n]: ").casefold()
	if usrin != "y".casefold():
		return False
	return True

def download(resdict, artist):
	u = '\"{}\"'.format(resdict['link'])
	q = '\"{}\"'.format(quality)
	s = '\"{}\"'.format(saveto)
	com = 'smloadr -q {qual} -p {save} -u {url}'.format(qual=q, save=s, url=u)
	name = ''
	if 'title' in resdict:
		name = resdict['title']
	if 'name' in resdict:
		name = resdict['name']
	print("Downloading {} - {}".format(artist, name))
	print("Will run the following command: {}".format(com))
	if confirm("Is this ok?"):
		subprocess.run(com, shell=True)

results = client.search(query=args.search, relation=args.type, limit=searchnum)
search = args.search.casefold()
namevar = ''
names = {'album': 'title', 'artist': 'name', 'track': 'title'}
namevar = names[args.type]
resultnames = {}
for result in results:
	resultnames[result.asdict()[namevar].casefold()] = result.asdict()

if search in resultnames:#check for exact(case-insensitive) match
	print("Found exact match: {} - {}".format(resultnames[search]['artist']['name'], search))
	if confirm("Download it?"):
		download(resultnames[search], resultnames[search]['artist']['name'])
		quit()
	print()

print("Results:")
i=0
indexednames = [] 
for name in resultnames:
	artist = resultnames[name]['artist']['name']
	indexednames.append(resultnames[name])
	print("{}: {} - {}".format(i, artist, name))
	i=i+1
selection = int(input("Enter number of your selection: "))
download(indexednames[selection], indexednames[selection]['artist']['name'])
